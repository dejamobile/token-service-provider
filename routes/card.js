var express = require('express');
var router = express.Router();
var randomToken = require('random-token');
var Datastore = require('nedb')
  , db = new Datastore();

router.get('/visa', function(req, res, next) {
  const token = randomToken(16);
  const result = {
    'cardNumber': req.query.cardNumber,
    'type':'visa',
    'token': token
  };
  db.insert(result);
  res.send(result);
});

router.get('/mastercard', function(req, res, next) {
  const token = randomToken(16);
  const result = {
    'cardNumber': req.query.cardNumber,
    'type':'mastercard',
    'token': token
  };
  db.insert(result);
  res.send(result);
});

router.get('/token/:token', function(req, res, next) {
  db.findOne({ token: req.params.token }, { type: 1, cardNumber: 1, _id: 0}, function (err, doc) {
    res.send(doc);
  });
});

module.exports = router;
