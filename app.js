var express = require('express');
var path = require('path');

var logger = require('morgan');

var cardRouter = require('./routes/card');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/card', cardRouter);

module.exports = app;
